module gitlab.com/frenchutouch/futify

go 1.16

replace github.com/nanu-c/qml-go => github.com/manland/qml-go v0.0.0-20220224211303-83edb7061659

replace github.com/librespot-org/librespot-golang => github.com/manland/librespot-golang v0.0.0-20220307212819-9a7a7de0ed4d

replace github.com/zmb3/spotify/v2 => github.com/manland/spotify/v2 v2.0.2-0.20220316203639-f8f2d3356c39

require (
	github.com/librespot-org/librespot-golang v0.0.0-20200423180623-b19a2f10c856
	github.com/miekg/dns v1.1.46 // indirect
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	github.com/zmb3/spotify/v2 v2.0.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/tools v0.1.9 // indirect
)
