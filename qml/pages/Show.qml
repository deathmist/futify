import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property ShowType show

    signal startShow(ShowType show)
    signal startTrack(var track)
    signal addToEndQueue(var track)

    header: PageHeader {
        id: header
        title: show.name
        StyleHints {
            dividerColor: UbuntuColors.green
        }
    }

    ShowView {
        id: playlistView
        show: page.show
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartShow: page.startShow(show)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
    }
}