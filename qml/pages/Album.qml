import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property AlbumType album

    signal startAlbum(AlbumType album)
    signal startTrack(var track)
    signal addToEndQueue(var track)

    header: PageHeader {
        id: header
        title: album.name
        StyleHints {
            dividerColor: UbuntuColors.green
        }
    }

    AlbumView {
        album: page.album
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartAlbum: page.startAlbum(album)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
    }
}