import QtQuick 2.12
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0

ListView {
    property var albums
    property var getIndex
    property int size: 0

    signal select(var selected)
    signal start(var selected)

    clip: true
    model: size
    delegate: ListItem {
        height: units.gu(10)
        width: parent.width

        property var album: getIndex(albums, index)

        Row {
            height: parent.height
            width: parent.width
            padding: units.gu(2)
            spacing: units.gu(2)

            Item {
                id: itemImage
                width: units.gu(6)
                height: units.gu(6)
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    id: image
                    source: album.image
                    width: parent.width
                    height: parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    fillMode: Image.PreserveAspectFit
                    visible: false
                }
                OpacityMask {
                    anchors.fill: image
                    source: image
                    width: image.width
                    height: image.height
                    maskSource: Rectangle {
                        width: image.width
                        height: image.height
                        radius: 5
                        visible: false // this also needs to be invisible or it will cover up the image
                    }
                }
            }
            Column {
                anchors.verticalCenter: itemImage.verticalCenter
                width: parent.width - itemImage.width - parent.spacing - parent.padding
                Label { text: album.name ; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: 1; textSize: Label.Large }
                Label { text: album.group ; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount:1; }
            }
        }
        onClicked: select(album)
        trailingActions: ListItemActions {
            actions: [
                Action {
                    iconName: "media-playback-start"
                    text: qsTr("Play")
                    onTriggered: start(album)
                }
            ]
        }
        
    }
}