package server

import (
	"fmt"
	"sort"
	"strings"

	"github.com/librespot-org/librespot-golang/librespot/utils"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify/v2"
)

// Playlists contain a list of album
type Playlists struct {
	Root      qml.Object
	Uuid      string
	Name      string
	Size      int
	playlists []*Playlist
}

func NewPlaylists(uuid string, name string, playlists []*Playlist) *Playlists {
	return &Playlists{
		Uuid:      uuid,
		Name:      name,
		Size:      len(playlists),
		playlists: playlists,
	}
}

// Playlist contain a list of tracks
type Playlist struct {
	Root      qml.Object
	Uuid      string
	Name      string
	Owner     string
	Image     string
	Followers int
	Size      int
	tracks    []*Track
}

func NewPlaylist(uuid string, name string, owner string, image string, followers uint, tracks []*Track) *Playlist {
	return &Playlist{
		Uuid:      uuid,
		Name:      name,
		Owner:     owner,
		Image:     image,
		Followers: int(followers),
		Size:      len(tracks),
		tracks:    tracks,
	}
}

// Album contain a list of track
type Album struct {
	Root   qml.Object
	Uuid   string
	Name   string
	Group  string
	Image  string
	Size   int
	tracks []*Track
}

func NewAlbum(uuid string, name string, group string, artists []spotify.SimpleArtist, image string, tracks []*Track) *Album {
	if group == "" {
		a := make([]string, len(artists))
		for index, artist := range artists {
			a[index] = artist.Name
		}
		group = strings.Join(a, " - ")
	}
	return &Album{
		Uuid:   uuid,
		Name:   name,
		Group:  group,
		Image:  image,
		Size:   len(tracks),
		tracks: tracks,
	}
}

// Track represent a track
type Track struct {
	Root       qml.Object
	Id         string
	Name       string
	Album      string
	Artist     string
	Image      string
	Genre      string
	Starred    bool
	Popularity int
	Duration   int
	Path       string
	Progress   uint32
	isEpisode  bool
}

func SelectImageURL(images []spotify.Image) string {
	var imageMin spotify.Image
	for _, img := range images {
		if imageMin.URL == "" {
			imageMin = img
		} else if img.Width < imageMin.Width && img.Width > 80 {
			imageMin = img
		}
	}
	//TODO default image if not found
	return imageMin.URL
}

func NewTrack(t *spotify.FullTrack, s *Session) *Track {
	artistsNames := make([]string, len(t.Artists))
	for i, a := range t.Artists {
		artistsNames[i] = a.Name
	}
	id := utils.Base62ToHex(strings.TrimPrefix(string(t.URI), "spotify:track:"))
	track := &Track{
		Id:         id,
		Name:       t.Name,
		Album:      t.Album.Name,
		Artist:     strings.Join(artistsNames, ", "),
		Image:      SelectImageURL(t.Album.Images),
		Genre:      t.Album.AlbumType,
		Starred:    false, // TODO find a way to know that
		Popularity: t.Popularity,
		Duration:   t.Duration,
		Path:       fmt.Sprintf("http://127.0.0.1:%d/song?id=%s", s.port, id),
		Progress:   0,
		isEpisode:  false,
	}
	s.tracks[id] = track
	return track
}

func NewEpisode(t *spotify.EpisodePage, s *Session) *Track {
	id := utils.Base62ToHex(strings.TrimPrefix(string(t.URI), "spotify:episode:"))
	track := &Track{
		Id:         id,
		Name:       t.Name,
		Album:      t.Description,
		Artist:     t.Show.Publisher,
		Image:      SelectImageURL(t.Images),
		Genre:      t.Type,
		Starred:    false, // TODO find a way to know that
		Popularity: -1,
		Duration:   t.Duration_ms,
		Path:       fmt.Sprintf("http://127.0.0.1:%d/song?id=%s", s.port, id),
		Progress:   0,
		isEpisode:  true,
	}
	s.tracks[id] = track
	return track
}

type Artist struct {
	Name       string
	Image      string
	Popularity int
}

func NewArtist(name string, image string, popularity int) *Artist {
	return &Artist{
		Name:       name,
		Image:      image,
		Popularity: popularity,
	}
}

// Shows contain a list of Show
type Shows struct {
	Root  qml.Object
	Uuid  string
	Name  string
	Size  int
	shows []*Show
}

func NewShows(uuid string, name string, shows []*Show) *Shows {
	return &Shows{
		Uuid:  uuid,
		Name:  name,
		Size:  len(shows),
		shows: shows,
	}
}

// Show contain a list of episodes
type Show struct {
	Root      qml.Object
	Uuid      string
	Name      string
	Owner     string
	Image     string
	Followers int
	Size      int
	tracks    []*Track
}

func NewShow(uuid string, name string, owner string, image string, followers uint, tracks []*Track) *Show {
	return &Show{
		Uuid:      uuid,
		Name:      name,
		Owner:     owner,
		Image:     image,
		Followers: int(followers),
		Size:      len(tracks),
		tracks:    tracks,
	}
}

// GetPlaylists return a playlists by id
func (s *Session) GetPlaylists(playlistID string) *Playlists {
	playlists, exist := s.playlists[playlistID]
	if !exist {
		fmt.Println("Playlists don't exsit " + playlistID)
		return nil
	}
	return playlists
}

// GetPlaylistByIndex return a playlist by index in Playslists
func (s *Session) GetPlaylistByIndex(playlistID string, index int) *Playlist {
	playlists, exist := s.playlists[playlistID]
	if !exist {
		fmt.Println("Playlist don't exsit (in GetPlaylistByIndex) " + playlistID)
		return nil
	}
	return playlists.playlists[index]
}

//GetPlaylist return a playslit from session cache
func (s *Session) GetPlaylist(playlistID string) *Playlist {
	playlist, exist := s.playlist[playlistID]
	if !exist {
		fmt.Println("Playlist don't exsit (in GetPlaylist) " + playlistID)
		return nil
	}
	return playlist
}

// GetAlbum return an album with albumID
func (s *Session) GetAlbum(albumID string) *Album {
	album, exist := s.albums[albumID]
	if !exist {
		fmt.Println("Album don't exsit (in GetAlbum) " + albumID)
		return nil
	}
	return album
}

// GetTrackByAlbum return a track with albumID and track index
func (s *Session) GetTrackByAlbum(albumID string, index int) *Track {
	album, exist := s.albums[albumID]
	if !exist {
		fmt.Println(fmt.Sprintf("Album don't exsit (in Get Track) %s width index=%d", albumID, index))
		return nil
	}
	if index >= len(album.tracks) {
		fmt.Println(fmt.Sprintf("Album don't have (in Get Track) index=%d width len=%d", index, len(album.tracks)))
		return nil
	}
	return album.tracks[index]
}

// GetTrack return a track with albumID and track index
func (s *Session) GetTrackByPlaylist(playlistID string, index int) *Track {
	playlist, exist := s.playlist[playlistID]
	if !exist {
		fmt.Println(fmt.Sprintf("Playlist don't exsit (in Get Track) %s width index=%d", playlistID, index))
		return nil
	}
	return playlist.tracks[index]
}

// GetShows return a shows by id
func (s *Session) GetShows(showID string) *Shows {
	shows, exist := s.shows[showID]
	if !exist {
		fmt.Println("Shows don't exsit " + showID)
		return nil
	}
	return shows
}

// GetShowByIndex return a show by index in Shows
func (s *Session) GetShowByIndex(showID string, index int) *Show {
	shows, exist := s.shows[showID]
	if !exist {
		fmt.Println("Show don't exsit (in GetShowByIndex) " + showID)
		return nil
	}
	return shows.shows[index]
}

//GetShow return a show from session cache
func (s *Session) GetShow(showID string) *Show {
	show, exist := s.show[showID]
	if !exist {
		fmt.Println("Show don't exsit (in GetShow) " + showID)
		return nil
	}
	return show
}

// GetTrackByShow return a track with showID and track index
func (s *Session) GetTrackByShow(showID string, index int) *Track {
	show, exist := s.show[showID]
	if !exist {
		fmt.Println(fmt.Sprintf("Show don't exsit (in GetTrackByShow) %s width index=%d", showID, index))
		return nil
	}
	return show.tracks[index]
}

type SearchResults struct {
	Root          qml.Object
	Tracks        []*Track
	SizeTracks    int
	Albums        []*Album
	SizeAlbums    int
	Playlists     []*Playlist
	SizePlaylists int
	Artists       []*Artist
	SizeArtists   int
	Episodes      []*Track
	SizeEpisodes  int
	Shows         []*Show
	SizeShows     int
}

func NewSearchResults(search *spotify.SearchResult, s *Session) *SearchResults {
	res := &SearchResults{}
	for _, item := range search.Albums.Albums {
		res.Albums = append(res.Albums, NewAlbum(item.ID.String(), item.Name, item.AlbumGroup, item.Artists, SelectImageURL(item.Images), []*Track{}))
	}
	for _, item := range search.Tracks.Tracks {
		res.Tracks = append(res.Tracks, NewTrack(&item, s))
	}
	for _, item := range search.Artists.Artists {
		res.Artists = append(res.Artists, NewArtist(item.Name, SelectImageURL(item.Images), item.Popularity))
	}
	for _, item := range search.Playlists.Playlists {
		res.Playlists = append(res.Playlists, NewPlaylist(item.ID.String(), item.Name, item.Owner.DisplayName, SelectImageURL(item.Images), item.Owner.Followers.Count, []*Track{}))
	}
	for _, item := range search.Episodes.Episodes {
		res.Episodes = append(res.Episodes, NewEpisode(&item, s))
	}
	for _, item := range search.Shows.Shows {
		res.Shows = append(res.Shows, NewShow(item.ID.String(), item.Name, item.Publisher, SelectImageURL(item.Images), 0, []*Track{}))
	}
	sort.Slice(res.Tracks, func(i, j int) bool {
		return res.Tracks[i].Popularity > res.Tracks[j].Popularity
	})
	sort.Slice(res.Artists, func(i, j int) bool {
		return res.Artists[i].Popularity > res.Artists[j].Popularity
	})
	sort.Slice(res.Playlists, func(i, j int) bool {
		return res.Playlists[i].Followers > res.Playlists[j].Followers
	})
	res.SizeTracks = len(res.Tracks)
	res.SizeAlbums = len(res.Albums)
	res.SizePlaylists = len(res.Playlists)
	res.SizeArtists = len(res.Artists)
	res.SizeEpisodes = len(res.Episodes)
	res.SizeShows = len(res.Shows)
	return res
}

func (s *Session) GetSearchResultTrack(index int) *Track {
	return s.searchResults.Tracks[index]
}

func (s *Session) GetSearchResultAlbum(index int) *Album {
	return s.searchResults.Albums[index]
}

func (s *Session) GetSearchResultPlaylist(index int) *Playlist {
	return s.searchResults.Playlists[index]
}

func (s *Session) GetSearchResultArtist(index int) *Artist {
	return s.searchResults.Artists[index]
}

func (s *Session) GetSearchResultEpisode(index int) *Track {
	return s.searchResults.Episodes[index]
}

func (s *Session) GetSearchResultShow(index int) *Show {
	return s.searchResults.Shows[index]
}
