/*
 * Copyright (C) 2020  Romain Maneschi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/nanu-c/qml-go"
	"gitlab.com/frenchutouch/futify/server"
)

func init() {
	if os.Getenv("XDG_CACHE_HOME") == "" {
		os.Setenv("XDG_RUNTIME_DIR", "/home/rmaneschi/go/src/futify/")
	}
}

func main() {
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {

	engine := qml.NewEngine()
	if err := qml.LoadTranslatorCurrentLocale("qml", "_", "i18n", ".qm"); err != nil {
		return err
	}
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}

	spotSession := server.NewSession(8765)
	player := server.NewHttpPlayer(spotSession)
	player.Start()

	fmt.Println("before create context")
	context := engine.Context()
	fmt.Println("before set spotSession")
	context.SetVar("spotSession", spotSession)

	fmt.Println("before create window")
	win := component.CreateWindow(context)
	fmt.Println("before set win root")
	spotSession.Root = win.Root()

	fmt.Println("before window show")
	win.Show()
	fmt.Println("before window wait")

	engine.Retranslate()

	win.Wait()

	return nil
}
